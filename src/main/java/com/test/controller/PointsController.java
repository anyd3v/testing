package com.test.controller;

import com.test.form.PointCreateForm;
import com.test.model.Point;
import com.test.services.PointService;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Controller
@RequestMapping("/points")
public class PointsController {

    @Autowired
    private PointService pointService;

    @RequestMapping(method = RequestMethod.POST)
    public String create(@Valid PointCreateForm pointForm, BindingResult result, RedirectAttributes redirectAttributes) {
        if(result.hasErrors()) {
            redirectAttributes.addAttribute("pointForm", pointForm);
            return "redirect:/";
        }

        pointService.save(new Point(pointForm));
        return "redirect:/";
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String show(@PathVariable("id")ObjectId id, Model model) {
        model.addAttribute("point", pointService.findById(id));
        return "point";
    }

}
