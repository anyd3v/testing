package com.test.controller;

import com.test.form.PointCreateForm;
import com.test.services.PointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/")
public class HomeController {

    @Autowired
    private PointService pointService;

    @RequestMapping(method = RequestMethod.GET)
    public String home(Model model) {
        model.addAttribute("points", pointService.getLast());
        if(!model.containsAttribute("pointForm"))
            model.addAttribute("pointForm", new PointCreateForm());
        return "home";
    }


}
