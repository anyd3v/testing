package com.test.form;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class PointCreateForm {

    @NotNull
    @Size(min = 1)
    private String address;
    @NotNull
    @Size(min = 1)
    private String name;
    @NotNull
    private double latitude;
    @NotNull
    private double longitude;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
