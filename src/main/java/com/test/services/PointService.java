package com.test.services;

import com.test.model.Point;
import org.bson.types.ObjectId;

import java.util.List;

public interface PointService {

    List<Point> getLast();

    void save(Point point);

    Point findById(ObjectId id);

}
