package com.test.services;

import com.test.model.Point;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PointServiceImpl implements PointService {

    private Class<Point> entityClass = Point.class;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Override
    public List<Point> getLast() {
        Query query = new Query().limit(10).with(new Sort(Sort.Direction.DESC, "createdAt"));
        return mongoTemplate.find(query, entityClass);
    }

    @Override
    public void save(Point point) {
        mongoTemplate.save(point);
    }

    @Override
    public Point findById(ObjectId id) {
        return mongoTemplate.findById(id, entityClass);
    }
}

