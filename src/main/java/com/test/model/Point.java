package com.test.model;

import com.test.form.PointCreateForm;
import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "points")
public class Point {

    @Id
    private ObjectId id;
    private String address;
    private String name;
    private Date createdAt;
    private double latitude;
    private double longitude;

    public Point() {}

    public Point(PointCreateForm pointForm) {
        this.address = pointForm.getAddress();
        this.name = pointForm.getName();
        this.createdAt = new Date();
        this.latitude = pointForm.getLatitude();
        this.longitude = pointForm.getLongitude();
    }

    public ObjectId getId() {
        return id;
    }

    public void setId(ObjectId id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
