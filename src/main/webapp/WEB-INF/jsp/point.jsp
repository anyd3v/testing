<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<jsp:include page="common/head.jsp"/>
<body>
<div class="container">
    <div class="row point">
        <div  class="col-sm-3">
            <div id="map_canvas"></div>
        </div>
        <div class="col-sm-9">
            <div class="form-horizontal">
                <div class="form-group">
                    <span class="col-sm-2 control-label">Название</span>
                    <div class="col-sm-7">
                        <span>${point.name}</span>
                    </div>
                </div>
                <div class="form-group">
                    <span for="address" class="col-sm-2 control-label">Адрес</span>
                    <div class="col-sm-7">
                        <span>${point.address}</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<jsp:include page="common/js.jsp"/>
<script type="text/javascript">
    $(document).ready(function() {
        var lat = ${point.latitude};
        var log = ${point.longitude};
        var map_canvas = document.getElementById('map_canvas');
        var map_options = {
            center: new google.maps.LatLng(lat, log),
            zoom: 6,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(map_canvas, map_options);

        new google.maps.Marker({
            position: new google.maps.LatLng(lat, log),
            map: map,
            title: '${point.name}'
        });
    });
</script>
</body>
</html>
