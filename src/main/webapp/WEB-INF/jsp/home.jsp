<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<jsp:include page="common/head.jsp"/>
<body>
<div class="container">
<div class="row">
<form:form action="/points" modelAttribute="pointForm" cssClass="form-horizontal">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Название</label>
        <div class="col-sm-6">
            <form:input id="name" path="name" cssClass="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">Адрес</label>
        <div class="col-sm-6">
            <form:input  id="address" path="address" cssClass="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">longitude</label>
        <div class="col-sm-6">
            <form:input id="longitude" path="longitude" cssClass="form-control" />
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">latitude</label>
        <div class="col-sm-6">
            <form:input id="latitude" path="latitude" cssClass="form-control" />
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button id="submitPoint" type="submit" class="btn btn-default">Создать</button>
        </div>
    </div>
</form:form>
</div>
<div class="row">
    <div  class="col-sm-3">
        <h3>Последние добавленные точки</h3>
        <ul>
        <c:forEach items="${points}" var="point">
            <li><a href="/points/${point.id}">${point.name}</a></li>
        </c:forEach>
        </ul>
</div>
    <div class="col-sm-9">
        <div id="map_canvas"></div>
    </div>
</div>
</div>
<jsp:include page="common/js.jsp"/>
<script type="text/javascript">

    $(document).ready(function() {
        var geocoder = new google.maps.Geocoder();

        $("#submitPoint").on("click", function(e) {
            var name = $("#name").val();
            var address = $("#address").val();
            var log = $("#longitude").val();
            var lat = $("#latitude").val();
            if(name === '' || address === '' || lat === '' || log === '') {
                e.preventDefault();
                alert("Введите корректные данные");
            }
        });
        var map_canvas = document.getElementById('map_canvas');
        var map_options = {
            center: new google.maps.LatLng(0, 0),
            zoom: 1,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(map_canvas, map_options);
        <c:forEach items="${points}" var="point">
            new google.maps.Marker({
                position: new google.maps.LatLng(${point.latitude}, ${point.longitude}),
                map: map,
                title: '${point.name}'
            });
        </c:forEach>
    });
</script>
</body>
</html>
